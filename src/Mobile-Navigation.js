/* eslint-disable jsx-a11y/anchor-is-valid */

import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faChevronRight,
  faChevronLeft,
} from "@fortawesome/free-solid-svg-icons";
import '../src/assets/scss/Mobile-Navigation.scss';

const MobileNavigation = ({ isShow, navs }) => {
  const onOpenMobileMenu = (target) => {
    const menu = document.querySelector(`[data-menu="${target}"]`);
    if (menu) {
      menu.closest(".list-wrapper").classList.add("is-active");
    }
  };

  const backToOneLevel = (event) => {
    event.target.closest(".list-wrapper").classList.remove("is-active");
  };

  const ListSubMenu = ({ menuName, subMenus }) => {
    return (
      <>
        <div className="list-wrapper">
          <div className="sub-menu-wrapper">
            <button
              className="nv-mobile-sub-menu__back"
              onClick={(e) => {
                backToOneLevel(e);
              }}
            >
              <span className="nv-mobile-sub-menu__back-icon">
                <FontAwesomeIcon icon={faChevronLeft}></FontAwesomeIcon>
              </span>
              Back
            </button>
            <ul data-menu={menuName} className="nv-mobile-sub-menu">
              {subMenus.map((subMenu, index) => {
                return (
                  <li
                    key={`${index}${Math.random()}`}
                    className="nv-mobile-sub-menu__item"
                  >
                    <a
                      onClick={() => {
                        onOpenMobileMenu(subMenu.name);
                      }}
                      data-target-menu={subMenu.name}
                      href="#"
                      className="nv-mobile-menu__link"
                    >
                      {subMenu.name}
                      {subMenu.hasChild && (
                        <span className="nv-mobile-menu-icon">
                          <FontAwesomeIcon
                            icon={faChevronRight}
                          ></FontAwesomeIcon>
                        </span>
                      )}
                    </a>
                    {subMenu.hasChild && (
                      <ListSubMenu
                        menuName={subMenu.name}
                        subMenus={subMenu.childs}
                      ></ListSubMenu>
                    )}
                  </li>
                );
              })}
            </ul>
          </div>
        </div>
      </>
    );
  };
  const ListMenu = ({ menus }) => {
    return (
      <>
        <div className="list-wrapper">
          <ul className="nv-mobile-menu">
            {menus.map((menu, index) => {
              return (
                <li
                  key={`${index}${Math.random()}`}
                  className="nv-mobile-menu__item"
                >
                  <a
                    onClick={() => {
                      onOpenMobileMenu(menu.name);
                    }}
                    data-target-menu={menu.name}
                    href="#"
                    className="nv-mobile-menu__link"
                  >
                    {menu.name}
                    {menu.hasChild && (
                      <span className="nv-mobile-menu-icon">
                        <FontAwesomeIcon
                          icon={faChevronRight}
                        ></FontAwesomeIcon>
                      </span>
                    )}
                  </a>
                  {menu.hasChild && (
                    <ListSubMenu
                      menuName={menu.name}
                      subMenus={menu.childs}
                    ></ListSubMenu>
                  )}
                </li>
              );
            })}
          </ul>
        </div>
      </>
    );
  };
  return (
    <>
      {/* <div className={`nv-mobile-menu-wrapper ${isShow ? "is-active" : ""}`}>
        <div className="list-wrapper">
          <ul className="nv-mobile-menu">
            <li className="nv-mobile-menu__item">
              <a
                onClick={() => {
                  onOpenMobileMenu("Phone");
                }}
                data-target-menu="Phone"
                href="#"
                className="nv-mobile-menu__link"
              >
                Phone
                <span className="nv-mobile-menu-icon">
                  <FontAwesomeIcon icon={faChevronRight}></FontAwesomeIcon>
                </span>
              </a>
            </li>
            <li className="nv-mobile-menu__item">
              <a href="#" className="nv-mobile-menu__link">
                Tablet
              </a>
            </li>
          </ul>
        </div>
        <div className="list-wrapper">
          <div className="sub-menu-wrapper">
            <button
              className="nv-mobile-sub-menu__back"
              onClick={(e) => {
                backToOneLevel(e);
              }}
            >
              <span className="nv-mobile-sub-menu__back-icon">
                <FontAwesomeIcon icon={faChevronLeft}></FontAwesomeIcon>
              </span>
              Back
            </button>
            <ul data-menu="Phone" className="nv-mobile-sub-menu">
              <li className="nv-mobile-sub-menu__item">
                <a
                  onClick={() => {
                    onOpenMobileMenu("Iphone");
                  }}
                  data-target-menu="Iphone"
                  href="#"
                  className="nv-mobile-menu__link"
                >
                  Apple
                </a>
              </li>
              <li className="nv-mobile-sub-menu__item">
                <a href="#" className="nv-mobile-sub-menu__link">
                  Samsung
                </a>
              </li>
            </ul>
          </div>
        </div>
        <div className="list-wrapper">
          <div className="sub-menu-wrapper">
            <button
              className="nv-mobile-sub-menu__back"
              onClick={(e) => {
                backToOneLevel(e);
              }}
            >
              <span className="nv-mobile-sub-menu__back-icon">
                <FontAwesomeIcon icon={faChevronLeft}></FontAwesomeIcon>
              </span>
              Back
            </button>
            <ul data-menu="Iphone" className="nv-mobile-sub-menu">
              <li className="nv-mobile-sub-menu__item">
                <a
                  onClick={() => {
                    onOpenMobileMenu(null);
                  }}
                  data-target-menu="#"
                  href="#"
                  className="nv-mobile-menu__link"
                >
                  Iphone 14 pro
                </a>
              </li>
              <li className="nv-mobile-sub-menu__item">
                <a href="#" className="nv-mobile-sub-menu__link">
                  Iphone 14 pro max
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div> */}
      <div className={`nv-mobile-menu-wrapper ${isShow ? "is-active" : ""}`}>
        <ListMenu menus={navs}></ListMenu>
      </div>
    </>
  );
};
export default MobileNavigation;
