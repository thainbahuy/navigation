import * as types from './constants';

export function getNavigationData() {
    return {
      type: types.GET_NAVIGATION,
    }
  }