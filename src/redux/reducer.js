import navigation from  "../dumpData/navigation.json";
import * as types from './constants';
const initState = {
    navigations: []
}

const reducer = function(state = initState, action) {
  switch (action.type) {
    case types.GET_NAVIGATION:
        return {
            ...state,
            navigations: navigation.navigations
        }
    default:
      return state;
  }
};

export default reducer;
