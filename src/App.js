/* eslint-disable jsx-a11y/anchor-is-valid */
import "./App.scss";
import React,{ useState, useEffect } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBars } from "@fortawesome/free-solid-svg-icons";
import Navigation from "./Navigation";
import MobileNavigation from "./Mobile-Navigation";
import { useSelector, useDispatch } from "react-redux";
import { getNavigationData } from "./redux/actions";
import {
  useWindowSize
} from '@react-hooks-library/core'

const App = () => {
  const  {navigations}  = useSelector(state => state.navigationReducer);
  const dispatch = useDispatch();
  const [isShow, setIsShow] = useState(false);
  const {width} = useWindowSize();
  const onClick = () => {
    if (isShow) {
      setIsShow(false);
    } else {
      setIsShow(true);
    }
  };
  const onClickContainer = () => {
    if (isShow) {
      setIsShow(false);
    }
  };
  useEffect(() => {
    dispatch(getNavigationData());
  }, []);

  return (
    <div className="App">
      <div className="nv-wrapper">
        <div className="nv-container">
          <div className="nv-header">
            <a className="nv-header__logo" href="#home">
              Logo
            </a>
            <Navigation navs={navigations} />
            <a className="nv-header__icon" onClick={onClick} href="#">
              <FontAwesomeIcon icon={faBars} />
            </a>
          </div>
          <MobileNavigation isShow={isShow} navs={navigations} />
          <div className="nv-content">
            <h3>Vertical Mobile Navbar</h3>
            <p>
              This example demonstrates how a navigation menu on a mobile/smart
              phone could look like.
            </p>
            <p>
              Click on the hamburger menu (three bars) in the top right corner,
              to toggle the menu.
            </p>
          </div>
        </div>
        <div
          onClick={onClickContainer}
          className={`js--overlay theme--dark is--closable ${
            isShow && width < 768 ? "is--open" : ""
          }`}
        />
      </div>
    </div>
  );
};

export default App;
