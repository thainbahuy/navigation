/* eslint-disable jsx-a11y/anchor-is-valid */
import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronRight } from "@fortawesome/free-solid-svg-icons";

const Navigation = ({ navs }) => {
  const ListSubMenu = ({ subMenus }) => {
    return (
      <ul className="nv-header__sub-menu">
        {subMenus.map((item, index) => {
          return (
            <li
              key={`${index}${Math.random()}`}
              className="nv-header__sub-menu-item"
            >
              <a className="nv-header__sub-menu-link" href="#">
                {item.name}
                {item.hasChild && (
                  <span className="nv-header__sub-menu-icon">
                    <FontAwesomeIcon icon={faChevronRight}></FontAwesomeIcon>
                  </span>
                )}
              </a>
              {item.hasChild && <ListSubMenu subMenus={item.childs} />}
            </li>
          );
        })}
      </ul>
    );
  };
  const ListMenu = ({ menus }) => {
    return (
      <>
        <ul className="nv-header__menu">
          {menus.map((menu, index) => {
            return (
              <li
                key={`${index}${Math.random()}`}
                className="nv-header__menu-item"
              >
                <a className="nv-header__menu-link" href={menu.name}>
                  {menu.name}
                </a>
                {menu.hasChild && <ListSubMenu subMenus={menu.childs} />}
              </li>
            );
          })}
        </ul>
      </>
    );
  };
  return (
    <>
      <ListMenu menus={navs} />
    </>
  );
};
export default Navigation;
